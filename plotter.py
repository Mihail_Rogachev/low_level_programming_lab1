import pandas as pd
import matplotlib.pyplot as plt
import sys

filename = "C://Users/Huawei/CLionProjects/llp/cmake-build-debug/add_del.csv"
title = "index"
df_add = pd.read_csv(filename)

plt.plot(df_add["index"], df_add["time"])
plt.title(title)
plt.xlabel("Count")
plt.ylabel("Time")
plt.grid(True)
plt.show()
